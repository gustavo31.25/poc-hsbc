import cv2
import matplotlib.pyplot as plt
import numpy as np
import json
from keras.preprocessing import image
from datetime import date, datetime
from pymongo import MongoClient
import datetime
id_faces = {}
id
client = MongoClient()
db = client['test-database']



def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime, date)):
        return obj.isoformat()
    raise TypeError ("Type %s not serializable" % type(obj))
    
def process_timeAttributes(time_and_attributes, idThread):
    idF = 0
    #with open("resultsTime"+str(idThread)+ ".json", 'w') as fp:
     #   json.dump(time_and_attributes, fp, default=str)
    for results in time_and_attributes:
        #print("======== Time Results of Unknown{}========".format(idF))
        timeCheckIn = datetime.datetime.now()
        timeCheckOut = datetime.datetime.now()
        r = False
        for k,v in results.items():
            if(k == "arrived"):
                if(v == "Havent arrived"):
                    r = False
                else:
                    r = True
                    timeCheckIn = v
            if(k == "last_time"):
                if(v == "Havent arrived"):
                    r = False 
                else:
                    r = True
                    timeCheckOut = v
            #print("========{} : {}========".format(k,v))
        if(r == True):
            td = timeCheckOut - timeCheckIn
            results["time_spend"] = str(td)
            #print(results["time_spend"])
        idF+=1
def processAll(time_and_attributes, emotions_saved, idThread):
    idF = 0
    
    predictionsDict = []
    namedb = db.camera1
    if(idThread == 0):
        namedb = db.camera1
    elif(idThread == 1):
        namedb = db.camera2
    elif(idThread == 2):
        namedb = db.camera3
    elif(idThread == 3):
        namedb = db.camera4
        
        
    for results in emotions_saved:
        predictions = 0
       
        h = namedb.find_one({"uid": idF})
        if h:
            for emotion,result in results.items():
                predictions+=result
            for emotion, result in results.items():
                if(predictions>0):
                    percentage = result * 100 / predictions
                    newvalues = { "$set": { emotion: percentage } }
                    namedb.update_one({"uid": idF}, newvalues)
                    #print('%s : %f percent' % (emotion, percentage))
                else:
                    newvalues = { "$set": { emotion: 0 } }
                    namedb.update_one({"uid": idF}, newvalues)
            
        else:
            #h["uid"] =
            auxF = {'uid':idF}
            for emotion,result in results.items():
                predictions+=result

            for emotion, result in results.items():
                if(predictions>0):
                    percentage = result * 100 / predictions
                    
                    auxF[emotion] = percentage
                    print('%s : %f percent' % (emotion, percentage))
                else:
                    auxF[emotion] = 0
                    print('No predictions for Unknown{}'.format(idF))
            
            namedb.insert_one(auxF)
        
        idF+=1
    idF = 0
    for results in time_and_attributes:
        for k,v in results.items():
            newvalues = { "$set": { k: v } }
            namedb.update_one({"uid": idF}, newvalues)
        idF+=1
    
            
            
          
def process_time(time_of_unknown):
    for k,v in time_of_unknown.items():
        print("======== Time of Unknown{}========".format(k))
        print(v)

def erase_appending(em):
    aux = []
    for i in range(len(em)):
        print(i)
        aux.append({"arrived":datetime.datetime.now(), "time_spend":" 2019-02-21 16:58:29.468270","id":i})
    print(len(aux))
    return aux

       
def erase_values(emotions_saved):
    for results in emotions_saved:
        for emotion,result in results.items():
            results[emotion]=0
        
def process_resultsTotal(listae, listat):
    return "Hola"
    
    
def process_results(emotions_saved, idThread):
    idF = 0
    predictionsDict = []
    for results in emotions_saved:
        predictions = 0
        predictionsDict.append({"id":idF,"angry":0, "disgust":0, "fear":0, "happy":0,
                                                "sad":0, "surprise":0, "neutral":0})
        print("======== Emotion Results of Unknown{}========".format(idF))
        print("Percentage of emotions detected on video")
        for emotion,result in results.items():
            predictions+=result
        #Here Put the conditional and the predictions
        
        for emotion, result in results.items():
            if(predictions>0):
                percentage = result * 100 / predictions
                predictionsDict[idF][emotion] = percentage
                print('%s : %f percent' % (emotion, percentage))
            else:
                print('No predictions for Unknown{}'.format(idF))
        idF+=1
    with open("results"+str(idThread)+ ".json", 'w') as fp:
        json.dump(predictionsDict, fp)
    
def load_image(image_path, grayscale=False, target_size=None):
    pil_image = image.load_img(image_path, grayscale, target_size)
    return image.img_to_array(pil_image)

def load_detection_model(model_path):
    detection_model = cv2.CascadeClassifier(model_path)
    return detection_model


def detect_faces(detection_model, gray_image_array):
    return detection_model.detectMultiScale(gray_image_array, 1.3, 5)

def draw_bounding_box(face_coordinates, image_array, color):
    x, y, w, h = face_coordinates
    cv2.rectangle(image_array, (x, y), (x + w, y + h), color, 2)

def apply_offsets(face_coordinates, offsets):
    x, y, width, height = face_coordinates
    x_off, y_off = offsets
    return (x - x_off, x + width + x_off, y - y_off, y + height + y_off)

def draw_text(coordinates, image_array, text, color, x_offset=0, y_offset=0,
                                                font_scale=2, thickness=2):
        
    x, y = coordinates[:2]
    cv2.putText(image_array, text, (x + x_offset, y + y_offset),
                cv2.FONT_HERSHEY_SIMPLEX,
                font_scale, color, thickness, cv2.LINE_AA)

def get_colors(num_classes):
    colors = plt.cm.hsv(np.linspace(0, 1, num_classes)).tolist()
    colors = np.asarray(colors) * 255
    return colors

