#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 12:26:41 2019

@author: gustavomendez
"""

import numpy as np
import cv2
import time
import requests
import threading
from threading import Thread, Event, ThreadError

class Cam():
    def start(self):
        self.thread.start()
        print("camera stream started")
        
    def __init__(self, url):
        self.stream = requests.get(url, stream=True)
        self.thread_cancelled = False
        self.thread = Thread(target=self.run)
        print("camera initialised")
             
    def run(self):
        bytes=''
        while not self.thread_cancelled:
          try:
            bytes+=self.stream.raw.read(1024)
            a = bytes.find('\xff\xd8')
            b = bytes.find('\xff\xd9')
            if a!=-1 and b!=-1:
              jpg = bytes[a:b+2]
              bytes= bytes[b+2:]
              img = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8),cv2.IMREAD_COLOR)
              cv2.imshow('cam',img)
              if cv2.waitKey(1) ==27:
                exit(0)
          except ThreadError:
            self.thread_cancelled = True
        
    def is_running(self):
        return self.thread.isAlive()
      
    
    def shut_down(self):
        self.thread_cancelled = True
        #block while waiting for thread to terminate
        while self.thread.isAlive():
          time.sleep(1)
        return True

  
    
if __name__ == "__main__":
  url = 'rtsp://admin:$d!c2501@10.85.232.69:554/Streaming/Channels/102'
  cam = Cam(url)
  cam.start()