#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 19 11:55:59 2019

@author: gustavomendez
"""

import face_recognition
import cv2
import numpy as np
import threading
from keras.models import load_model
from statistics import mode
from utils.datasets import get_labels
from utils.inference import draw_text
from utils.inference import draw_bounding_box
from utils.inference import apply_offsets
from utils.inference import process_results
from utils.inference import erase_values
from utils.inference import process_time
from utils.inference import erase_appending
from utils.inference import processAll
from utils.inference import process_timeAttributes
from utils.preprocessor import preprocess_input
import datetime

#from utils.inference import known_face_encodings

class POCFace():#threading.Thread):
    def __init__(self, shared_general, shared_name, thread_id):
        #threading.Thread.__init__(self)
        
        self._known_face_encodings = shared_general
        self._known_face_names = shared_name
        
        # parameters for loading data and images
        self.emotion_model_path = './models/emotion_model.hdf5'
        self.emotion_labels = get_labels('fer2013')
        
        self.frame_window = 10
        self.emotion_offsets = (5, 10)
        
        # loading models
        self.emotion_classifier = load_model(self.emotion_model_path)
        # getting input model shapes for inference
        self.emotion_target_size = self.emotion_classifier.input_shape[1:3]
        
        # starting lists for calculating modes
        self.emotion_window = []
        
        
        self._emotions_saved = []
        #process_results(self._emotions_saved,self.thread_id)
        #erase_values(self._emotions_saved)
        
        self._time_and_attributes = []#erase_appending(shared_ta)
        #process_timeAttributes(self._time_and_attributes, self.thread_id)
        #erase_values(self._time_and_attributes)
        
        self.thread_id = thread_id
        self._time_of_unknown = []
        # Initialize some variables
        self.face_locations = []
        self.face_encodings = []
        # Get a reference to webcam #0 (the default one)
        #RTSP = 554
        #rtsp://admin:$d!c2501@10.85.232.69:554/Streaming/Channels/101
        self.video_capture = []
        for x in range(thread_id):
            if(x == 0):
                self.video_capture.append(cv2.VideoCapture(0)) 
            elif (x == 1):
                self.video_capture.append(cv2.VideoCapture("http://10.85.30.129:8080/video"))
            elif(x == 2):
                self.video_capture.append(cv2.VideoCapture("http://10.85.30.117:8080/video"))
            elif(x == 3):
                self.video_capture.append(cv2.VideoCapture("http://10.85.30.58/live"))
            else:
                self.video_capture.append(cv2.VideoCapture(1))
            
            self.emotion_window.append([])
            self._emotions_saved.append([])
            self._time_and_attributes.append([])
            self.face_locations.append([])
            self.face_encodings.append([])
            self._time_of_unknown.append({})
        
        #self.video_capture.open("rtsp://admin:$d!c2501@10.85.232.69:554/Streaming/Channels/101")
        
        self.face_names = []
        self.process_this_frame = True
        self.id_name = 0
        self.processIt()
        
        
    #def run(self):
       #self.processIt()
        
            
        
    def processIt(self):
        while True:
            
            for x in range(len(self.video_capture)):
                # Grab a single frame of video
            
                    
                    #sprint([li for li in self.emotion_window if li])
                ret, frame = self.video_capture[x].read()
                try:
                    gray_image = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
                    rgb_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                except:
                    continue
                
                # Resize frame of video to 1/4 size for faster face recognition processing
                small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
                
                # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
                rgb_small_frame = small_frame[:, :, ::-1]
                
                # Only process every other frame of video to save time
                if self.process_this_frame:
                    
                    # Find all the faces and face encodings in the current frame of video
                    self.face_locations[x] = face_recognition.face_locations(rgb_small_frame)
                    self.face_encodings[x] = face_recognition.face_encodings(rgb_small_frame, self.face_locations[x])
                    
                    self.face_names = []
                    for face_encoding in self.face_encodings[x]:
                        # See if the face is a match for the known face(s)
                        if len(self._known_face_encodings)> 0 :
                            matches = face_recognition.compare_faces(self._known_face_encodings, face_encoding)#,tolerance=0.80)
                        else:
                            matches = face_recognition.compare_faces(self._known_face_encodings, face_encoding)#,tolerance=0.80)
                            
                        self.id_name = len(self._known_face_encodings)
                        name = "Unknown"
                        name += str(self.id_name)
                        if True in matches:
                            first_match_index = matches.index(True)
                            name = self._known_face_names[first_match_index]
                        else:
                            self._known_face_encodings.append(face_encoding)
                            self._known_face_names.append(name)
                        
                        self.face_names.append(name)
                        
                
                
                
                # Display the results
                for (top, right, bottom, left), name in zip(self.face_locations[x], self.face_names):
                    # Scale back up face locations since the frame we detected in was scaled to 1/4 size
                    top *= 4
                    right *= 4
                    bottom *= 4
                    left *= 4
                    
                    x1, x2, y1, y2 = apply_offsets((left, top, right-left, bottom-top), self.emotion_offsets)
                    gray_face = gray_image[y1:y2, x1:x2]
                    try:
                        gray_face = cv2.resize(gray_face, (self.emotion_target_size))
                    except:
                        continue
                    
                    gray_face = preprocess_input(gray_face, True)
                    gray_face = np.expand_dims(gray_face, 0)
                    gray_face = np.expand_dims(gray_face, -1)
                    
                    emotion_prediction = self.emotion_classifier.predict(gray_face)
                    emotion_probability = np.max(emotion_prediction)
                    emotion_label_arg = np.argmax(emotion_prediction)
                    emotion_text = self.emotion_labels[emotion_label_arg]
                    self.emotion_window[x].append(emotion_text)
                    if len(self.emotion_window[x]) > self.frame_window:
                        self.emotion_window[x].pop(0)
                    try:
                        emotion_mode = mode(self.emotion_window[x])
                    except:
                        continue
                    
                    get_id = int(name[7:])
                    if(len([i for i in self._emotions_saved[x] if i])<(get_id+1)):
                        
                        for n in range((get_id+1)-len([l for l in self._emotions_saved[x] if l])):
                            self._emotions_saved[x].append({"angry":0, "disgust":0, "fear":0, "happy":0,"sad":0, "surprise":0, "neutral":0})
                            print("Que pex -------")
                            print((get_id+1)-len([li for li in self._time_and_attributes[x] if li])-1)
                            if(n == ((get_id+1)-len([li for li in self._time_and_attributes[x] if li])-1)):
                                self._time_of_unknown[x][name[7:]]=0
                                self._time_of_unknown[x][name[7:]]+=0.3005
                                id_aux = (get_id-n)
                                
                                self._time_and_attributes[x].append({"arrived":datetime.datetime.now(), "time_spend":self._time_of_unknown[x][name[7:]],"id":(id_aux), "last_time":datetime.datetime.now()})
                            else:
                                id_aux = n
                                
                                self._time_and_attributes[x].append({"arrived":"Havent arrived", "time_spend":0,"id":(id_aux), "last_time":"Havent arrived"}) 
        
                        self._emotions_saved[x][get_id][emotion_text] +=1
                        
                    else:
                        self._emotions_saved[x][get_id][emotion_text] +=1
                        if(name[7:] in self._time_of_unknown[x]):
                            self._time_of_unknown[x][name[7:]]+=0.3005
                            
                            
                        else:
                            self._time_of_unknown[x][name[7:]]=0
                            self._time_of_unknown[x][name[7:]]+=0.3005
                            
                        if(name[7:] in self._time_of_unknown[x]):
                            if(self._time_and_attributes[x][int(name[7:])]["arrived"] == "Havent arrived"):
                                self._time_and_attributes[x][int(name[7:])]["arrived"]= datetime.datetime.now()
                                self._time_and_attributes[x][int(name[7:])]["id"] = int(name[7:])
                                self._time_and_attributes[x][int(name[7:])]["last_time"] = datetime.datetime.now()
                            self._time_and_attributes[x][int(name[7:])]["time_spend"] = self._time_of_unknown[x][name[7:]]
                            self._time_and_attributes[x][int(name[7:])]["last_time"] = datetime.datetime.now()
                            
                        #else:
                            #self._time_and_attributes.append({"arrived":datetime.datetime.now(), "time_spend":self._time_of_unknown[name[7:]],
                             #                             "id":get_id})
                        
                    
                    # Draw a box around the face
                    cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
                    # Draw a label with a name below the face
                    cv2.rectangle(frame, (left, bottom - 25), (right, bottom), (0, 0, 255), cv2.FILLED)
                    font = cv2.FONT_HERSHEY_DUPLEX
                    cv2.putText(frame, name+" "+emotion_text, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
                
                
                # Display the resulting image
                cv2.imshow(str(x), frame)
                # Hit 'm' on the keyboard to show results!
                # Hit 'q' on the keyboard to quit!
                #process_results(self._emotions_saved[x],x)
                #process_time(self._time_of_unknown[x])
                process_timeAttributes(self._time_and_attributes[x], x)
                processAll(self._time_and_attributes[x], self._emotions_saved[x], x)
                k = cv2.waitKey(33)
                if k == ord('q'):
                    process_results(self._emotions_saved[x],x)
                    process_time(self._time_of_unknown[x])
                    process_timeAttributes(self._time_and_attributes[x], x)
                    processAll(self._time_and_attributes[x], self._emotions_saved[x], x)
                    return False
                    break
                elif k == ord('m'):
                    process_results(self._emotions_saved[x],x)
                    process_timeAttributes(self._time_and_attributes[x], x)
                    process_time(self._time_of_unknown[x])
                    processAll(self._time_and_attributes[x], self._emotions_saved[x], x)
            self.process_this_frame = not self.process_this_frame
        # Release handle to the webcam
        self.video_capture.release()
        cv2.destroyAllWindows()
        
        
        

        
    


